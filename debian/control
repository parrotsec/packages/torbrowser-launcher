Source: torbrowser-launcher
Maintainer: Debian Privacy Tools Maintainers <pkg-privacy-maintainers@lists.alioth.debian.org>
Uploaders: Roger Shimizu <rosh@debian.org>
Section: contrib/web
Priority: optional
Build-Depends:
 debhelper-compat (= 12),
 dh-apparmor,
 dh-python,
 help2man,
 lsb-release,
 python3-all,
 python3-distro,
 python3-gpg,
 python3-packaging,
 python3-pyqt5,
 python3-requests
Standards-Version: 4.5.0
Rules-Requires-Root: no
Homepage: https://github.com/micahflee/torbrowser-launcher
Vcs-Git: https://salsa.debian.org/pkg-privacy-team/torbrowser-launcher.git
Vcs-Browser: https://salsa.debian.org/pkg-privacy-team/torbrowser-launcher

Package: torbrowser-launcher
Architecture: i386 amd64
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ca-certificates,
 gnupg,
 libdbus-glib-1-2,
 python3-gpg,
 python3-packaging,
 python3-pyqt5,
 python3-requests,
 python3-socks
Recommends: tor
Suggests: apparmor
Description: helps download and run the Tor Browser Bundle
 Tor Browser Launcher is intended to make the Tor Browser Bundle (TBB) easier
 to maintain and use for GNU/Linux users. torbrowser-launcher handles
 downloading the most recent version of TBB for you, in your language and for
 your architecture. It also adds a "Tor Browser" application launcher to your
 operating system's menu.
 .
 When you first launch Tor Browser Launcher, it will download TBB from
 https://www.torproject.org/ and extract it to ~/.local/share/torbrowser,
 and then execute it.
 Cache and configuration files will be stored in ~/.cache/torbrowser and
 ~/.config/torbrowser.
 Each subsequent execution after installation will simply launch the most
 recent TBB, which is updated using Tor Browser's own update feature.
 .
